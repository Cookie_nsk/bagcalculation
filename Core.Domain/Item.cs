﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class Item
    {
        public string Name { get; set; }
        public int Cost { get; set; }
        public int Weight { get; set; }
    }
}
