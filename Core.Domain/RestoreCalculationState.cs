﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class RestoreCalculationState
    {
        public int RestoredI { get; set; }
        public int RestoredJ { get; set; }
        public int RestoredBestPosition { get; set; }
        public double RestoredBestValue { get; set; }
    }
}
