﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class Task
    {
        public string Guid { get; set; }
        public TaskState State { get; set; }
        public double CompletePercentage { get; set; }
        public List<Item> InputConditions { get; set; }
        public List<Item> ResultSet { get; set; }
        public int BagVolume { get; set; }
        public int BagOverallCost
        {
            get
            {
                if (ResultSet != null)
                {
                    return ResultSet.Sum(x => x.Cost);
                }

                return 0;
            }
        }
        public string StateString
        {
            get
            {
                switch (State)
                {
                    case TaskState.Canceled:
                        return "Отменена";
                    case TaskState.Done:
                        return "Завершена";
                    case TaskState.InProgress:
                        return "В процессе";
                    default:
                        return "Отменена";
                }
            }
        }
        public string CompletePercentageString
        {
            get
            {
                if (CompletePercentage > 100)
                {
                    return "100";
                }
                else
                {
                    return Math.Round(CompletePercentage, 2).ToString();
                }
            }
        }
        public int OverallVolume
        {
            get
            {
                if (ResultSet != null)
                {
                    return ResultSet.Sum(x => x.Weight);
                }

                return 0;
            }
        }
        public RestoreCalculationState RestoreData { get; set; }
        public bool WasRestored { get; set; }
    }
}
