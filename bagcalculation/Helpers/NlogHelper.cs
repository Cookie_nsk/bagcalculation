﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BagCalculation.Helpers
{
    public static class NlogHelper
    {
        public static void LogError(Exception exp, string comment = null)
        {
            LogManager.GetCurrentClassLogger().Error(exp, comment);
        }
    }
}