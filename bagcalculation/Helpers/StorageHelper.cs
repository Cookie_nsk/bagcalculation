﻿using Core.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Xml.Serialization;

namespace BagCalculation.Helpers
{
    public class StorageHelper
    {
        private static string ServerStorageDirectory
        {
            get { return System.Web.Hosting.HostingEnvironment.MapPath("~/Storage/Tasks"); }
        }
        public static List<Task> GetSavedTasks()
        {
            List<Task> tasks = new List<Task>();

            CheckForDirectory();

            foreach (var file in Directory.GetFiles(ServerStorageDirectory))
            {
                using (FileStream fileStream = File.Open(file, FileMode.Open))
                {
                    bool wasExc = false;
                    try
                    {
                        var serializer = new XmlSerializer(typeof(Task));
                        tasks.Add((Task)serializer.Deserialize(fileStream));
                        fileStream.Close();
                    }
                    catch (Exception exp)
                    {
                        NlogHelper.LogError(exp, "Trying to restore previos version of a file");
                        wasExc = true;
                    }
                    finally
                    {
                        fileStream.Close();
                    }
                    if (wasExc)
                    {
                        try
                        {
                            string text = File.ReadAllText(file);
                            string xml = text.Substring(0, text.Length - (text.Length - text.LastIndexOf("</Task>") - "</Task>".Length));
                            var serializer = new XmlSerializer(typeof(Task));
                            tasks.Add((Task)serializer.Deserialize(new MemoryStream(Encoding.UTF8.GetBytes(xml))));
                        }
                        catch (Exception exp)
                        {
                            NlogHelper.LogError(exp, string.Format("Restoring failed. File corrupted:{0}", file));
                        }
                    }
                }
            }

            return tasks;
        }
        public static void SaveTask(Task task)
        {
            CheckForDirectory();
            using (FileStream file = File.Open(Path.Combine(ServerStorageDirectory, task.Guid), FileMode.OpenOrCreate))
            {
                var serializer = new XmlSerializer(typeof(Task));
                serializer.Serialize(file, task);
                file.Close();
            }


        }
        public static void DeleteTask(string guid)
        {
            string path = Path.Combine(ServerStorageDirectory, guid);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
        private static void CheckForDirectory()
        {
            if (!Directory.Exists(ServerStorageDirectory))
            {
                Directory.CreateDirectory(ServerStorageDirectory);
            }
        }
    }
}