﻿using Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Tasks = System.Threading.Tasks;

namespace BagCalculation.Helpers
{
    public static class TasksHelper
    {
        public static List<Task> Tasks { get; set; }
        public static Dictionary<string, CancellationTokenSource> CancellationTokens
        {
            get;
            set;
        }
        static TasksHelper()
        {
            CancellationTokens = new Dictionary<string, CancellationTokenSource>();
            Tasks = new List<Task>();
        }
        public static void InitTasks()
        {
            Tasks = StorageHelper.GetSavedTasks();

            if (Tasks != null && Tasks.Count > 0)
            {
                foreach (Task task in Tasks.Where(x => x.State == TaskState.InProgress))
                {
                    CancellationTokenSource cToken = new CancellationTokenSource();
                    CancellationTokens.Add(task.Guid, cToken);
                    task.WasRestored = true;
                    new Tasks.TaskFactory().StartNew(() => CalculateBagContents(task, cToken.Token), cToken.Token);
                }
            }
        }
        public static void CreateNewTask(int bagVolume, int itemsQuantity)
        {
            Task task = new Task()
            {
                Guid = Guid.NewGuid().ToString(),
                State = TaskState.InProgress,
                BagVolume = bagVolume,
                InputConditions = new List<Item>(),
                ResultSet = new List<Item>(),
                RestoreData = new RestoreCalculationState(),
                WasRestored = false,
            };

            Random rnd = new Random();

            for (int i = 0; i < itemsQuantity; i++)
            {
                Item item = new Item()
                {
                    Cost = rnd.Next(1, 50),
                    Name = Guid.NewGuid().ToString(),
                    Weight = rnd.Next(1, 8),
                };

                task.InputConditions.Add(item);
            }

            CancellationTokenSource cToken = new CancellationTokenSource();
            CancellationTokens.Add(task.Guid, cToken);
            Tasks.Add(task);
            StorageHelper.SaveTask(task);

            new Tasks.TaskFactory().StartNew(() => CalculateBagContents(task, cToken.Token), cToken.Token);
            SendTaskState(task);
        }
        public static bool CancelTask(string taskGuid)
        {
            if (CancellationTokens.ContainsKey(taskGuid) && Tasks.Count(x => x.Guid.Equals(taskGuid)) > 0)
            {
                SendTaskState(Tasks.First(x => x.Guid.Equals(taskGuid)));
                CancellationTokens.First(x => x.Key.Equals(taskGuid)).Value.Cancel();
                Tasks.Remove(Tasks.First(x => x.Guid.Equals(taskGuid)));
                return true;
            }

            return false;
        }
        private static void CalculateBagContents(Task task, CancellationToken token)
        {
            double bestValue = task.WasRestored ? task.RestoreData.RestoredBestValue : 0d;
            int bestPosition = task.WasRestored ? task.RestoreData.RestoredBestPosition : 0;

            int size = task.InputConditions.Count;
            int permutations = (int)Math.Pow(2, size);

            for (var i = task.RestoreData.RestoredI; i < permutations; i++)
            {
                task.RestoreData.RestoredI = i;

                if (token.IsCancellationRequested)
                {
                    CancellationTokens.Remove(task.Guid);
                    task.State = TaskState.Canceled;
                    StorageHelper.DeleteTask(task.Guid);
                    SendTaskState(task);
                    return;
                }

                double total = 0d;
                double weight = 0d;

                for (var j = 0; j < size; j++)
                {
                    if (((i >> j) & 1) != 1) continue;

                    total += task.InputConditions[j].Cost;
                    weight += task.InputConditions[j].Weight;
                }

                if (weight <= task.BagVolume && total > bestValue)
                {
                    task.RestoreData.RestoredBestValue = total;
                    task.RestoreData.RestoredBestPosition = i;

                    bestPosition = i;
                    bestValue = total;
                }

                task.CompletePercentage += (100d / permutations);

                SendTaskState(task);

                StorageHelper.SaveTask(task);
            }

            for (var j = task.RestoreData.RestoredJ; j < size; j++)
            {
                task.RestoreData.RestoredJ = j;

                if (((bestPosition >> j) & 1) == 1)
                {
                    task.ResultSet.Add(task.InputConditions[j]);
                }
            }

            task.State = TaskState.Done;
            StorageHelper.SaveTask(task);

            SendTaskState(task);

        }
        private static void SendTaskState(Task task)
        {
            double percentage = Math.Round(task.CompletePercentage, 2);

            var context = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            context.Clients.All.displayMessage($"{task.Guid};{(percentage > 100d ? 100 : percentage)};{task.StateString}");
        }
    }
}