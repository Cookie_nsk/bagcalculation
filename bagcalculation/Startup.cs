﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(BagCalculation.Helpers.Startup))]

namespace BagCalculation.Helpers
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}